package org.acidbase.ei.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import jdk.nashorn.api.scripting.ClassFilter;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.acidbase.data.Entity;
import org.acidbase.data.EntityInfo;
import org.acidbase.data.IndexStructure;
import org.acidbase.data.IndexStructureCache;
import org.acidbase.data.mapper.Connector;
import org.acidbase.data.mapper.EntityMapper;
import org.acidbase.data.mapper.Mapper;
import org.acidbase.data.structure.Structure;
import org.acidbase.data.structure.WritingStructure;
import org.acidbase.ei.Main;
import org.acidbase.ei.RequestCollection;
import org.acidbase.ei.cache.ConnectorCache;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptEngine;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * The class to process jobs of type RMQ_ROUTE_KEY_INDEX
 * Each job description is a JSON in the following format:
 * { "db": "database name", "id": ID-OF-TABLE-notify_command }
 */
public class ReindexJob implements Job
{
    static Logger logger = LoggerFactory.getLogger(ReindexJob.class);

    protected Client es_client;
    static final SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
    static final SimpleDateFormat datetime_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Map<String, ScriptEngine> scriptEngines = new HashMap<>();

    public ReindexJob(Client esClient)
    {
        es_client = esClient;

        //Initializing a Nashorn Script Engine
        {
            ClassFilter cf = new ClassFilter() {
                @Override
                public boolean exposeToScripts(String s) {
                    return false;
                }
            };

            NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
            ScriptEngine engine = factory.getScriptEngine(cf);

            for (String name : factory.getNames()) {
                scriptEngines.put(name, engine);
            }
            if (!scriptEngines.containsKey(factory.getEngineName())) {
                scriptEngines.put(factory.getEngineName(), engine);
            }

            logger.info("Initialized Nashorn script engine");
        }
    }

    private Connector connector;
    private EntityMapper mapper;
    private String dbName;

    @Override
    public void execute(String json)
        throws Exception
    {
        Integer transactionKey = -2;
        try {
            JSONParser parser = new JSONParser();
            JSONObject payload = (JSONObject) parser.parse(json);
            dbName = payload.get("db").toString();
            JSONArray dataArray = (JSONArray) payload.get("data");

            connector = ConnectorCache.getConnector(dbName);
            mapper = new EntityMapper(connector);
            RequestCollection requestCollection = new RequestCollection();

            transactionKey = mapper.beginTransaction();

            //For profiling
            List<String> profiledIndices = new ArrayList<>();

            for (int i = 0; i < dataArray.size(); i++) {
                JSONObject data = (JSONObject) dataArray.get(i);

                Long entity_id = (Long) data.get("entity_id");
                Long revision_id = (Long) data.get("revision_id");
                String entity_type = (String) data.get("entity_type");
                String action = (String) data.get("action");
                String index_name = (String) data.get("index_name");

                if (Main.PROFILE) {
                    profiledIndices.add(entity_type);
                    if (!Main.profileTimestamps.containsKey("start_" + entity_type)) {
                        Main.profileTimestamps.put("start_" + entity_type, System.currentTimeMillis());
                    }
                }

                if (entity_id == null
                || revision_id == null
                || entity_type == null
                || action == null) {
                    return;
                }

                IndexStructure.ActionType action_type = null;
                switch (action) {
                    case "Add":
                        action_type = IndexStructure.ActionType.Add;
                        break;
                    case "Delete":
                        action_type = IndexStructure.ActionType.Delete;
                        break;
                    case "Revise":
                        action_type = IndexStructure.ActionType.Revise;
                        break;
                    case "Revive":
                        action_type = IndexStructure.ActionType.Revive;
                        break;
                    case "Edit":
                        action_type = IndexStructure.ActionType.Edit;
                        break;
                    case "Purge":
                        action_type = IndexStructure.ActionType.Purge;
                        break;
                }

                if (index_name == null || index_name.equals("")) {
                    firstPass(requestCollection, entity_id, revision_id, entity_type, action_type);
                }
                else {
                    //If the index is specified, work on that only
                    Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
                    if (all_indices.containsKey(index_name)) {
                        IndexStructure es = all_indices.get(index_name);
                        UpdateRequest ur = insertEntityIntoIndex(entity_id, revision_id, es);
                        if (ur != null) {
                            requestCollection.add(ur);
                        }
                    }
                }
            }

            if (requestCollection.size() > 0) {
                int startingRequest = 0;
                int counter = 0;
                BulkResponse bulkResponse = null;
                do {
                    if (bulkResponse != null) {
                        for (BulkItemResponse response : bulkResponse) {
                            if (response.isFailed() || response.getResponse().getShardInfo().getFailed() > 0) {
                                startingRequest = response.getItemId();
                                break;
                            }
                        }
                    }

                    BulkRequestBuilder bulkRequest = es_client.prepareBulk();
                    requestCollection.transferTo(bulkRequest, startingRequest);
                    bulkResponse = bulkRequest.get();
                    counter++;
                } while (bulkResponse.hasFailures() && counter <= 3);

                if (bulkResponse.hasFailures()) {
                    //TODO: There are requests that were not sent even after 3 tries, do something about them
                }
            }

            //Profiling
            for (String i : profiledIndices) {
                Main.profileTimestamps.put("end_" + i, System.currentTimeMillis());
            }

            mapper.commit(transactionKey);
            mapper.close();
        }
        catch (Exception ex) {
            if (mapper != null) {
                mapper.rollback(transactionKey);
            }
            throw ex;
        }
    }

    private void firstPass(RequestCollection bulkRequest, Long entity_id, Long revision_id, String type, IndexStructure.ActionType action_type)
        throws SQLException, org.json.simple.parser.ParseException, ExecutionException, JsonProcessingException
    {
        //Here indices are updated that have the same entity_type as the modified entity
        if (action_type == IndexStructure.ActionType.Add
        || action_type == IndexStructure.ActionType.Revise
        || action_type == IndexStructure.ActionType.Revive
        || action_type == IndexStructure.ActionType.Edit) {
            Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
            for (Map.Entry<String, IndexStructure> ai : all_indices.entrySet()) {
                IndexStructure es = ai.getValue();
                if (es.readingStructure.entity_type.equals(type)) {
                    UpdateRequest ur = insertEntityIntoIndex(entity_id, revision_id, es);
                    if (ur != null) {
                        bulkRequest.add(ur);
                    }
                }
            }
        }
        else if (action_type == IndexStructure.ActionType.Delete) {
            Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
            for (Map.Entry<String, IndexStructure> ai : all_indices.entrySet()) {
                IndexStructure es = ai.getValue();
                if (es.writingStructure.entity_type.equals(type)) {
                    if (es.readingStructure.revision_state == Structure.RevisionStateType.Latest) {
                        UpdateRequest ur = insertEntityIntoIndex(entity_id, revision_id, es);
                        if (ur != null) {
                            bulkRequest.add(ur);
                        }
                    }
                    else if (es.readingStructure.revision_state == Structure.RevisionStateType.LatestActive) {
                        DeleteRequest dr = removeEntityFromIndex(entity_id, revision_id, es);
                        bulkRequest.add(dr);
                    }
                }
            }
        }
        else if (action_type == IndexStructure.ActionType.Purge) {
            Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
            for (Map.Entry<String, IndexStructure> ai : all_indices.entrySet()) {
                IndexStructure es = ai.getValue();
                if (es.readingStructure.entity_type.equals(type)) {
                    DeleteRequest dr = removeEntityFromIndex(entity_id, revision_id, es);
                    bulkRequest.add(dr);
                }
            }
        }

        //Now it's time to update indices which are affected by the change but are not of the same entity_type
        if (action_type == IndexStructure.ActionType.Delete
        || action_type == IndexStructure.ActionType.Revise
        || action_type == IndexStructure.ActionType.Edit
        || action_type == IndexStructure.ActionType.Purge) {
            //In case the entity is not new, it's much simpler to handle
            // find all the indices which mention the changed entity
            Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
            for (Map.Entry<String, IndexStructure> ai : all_indices.entrySet()) {
                searchAndReindexByOneLevel(bulkRequest, entity_id, revision_id, type, ai.getValue());
            }
        }
        else if (action_type == IndexStructure.ActionType.Add
        || action_type == IndexStructure.ActionType.Revive) {
            //But if the entity is new (added or revived), it's a little harder
            // First we take a step back in hierarchy and find the direct neighbour nodes
            List<EntityInfo> affected_entities = mapper.getRelatedRevisions(entity_id, Mapper.LockType.NoLock);
            for (EntityInfo ei : affected_entities) {
                // Then we will update the neighbour node as if it was modified
                // We do this for the indices with the structure: Neighbour.EntityType
                Map<String, IndexStructure> all_indices = IndexStructureCache.getInstance(connector).getIndexStructures();
                for (Map.Entry<String, IndexStructure> ai : all_indices.entrySet()) {
                    searchAndReindexByTwoLevels(bulkRequest, ei.entity_id, ei.revision_id, ei.entity_type, type, ai.getValue());
                }
            }
        }
    }

    private DeleteRequest removeEntityFromIndex(Long entity_id, Long revision_id, IndexStructure index_structure)
    {
        String id = "";

        if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Latest) {
            id = entity_id.toString();
        }
        else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.LatestActive) {
            id = entity_id.toString();
        }
        else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Revision) {
            id = revision_id.toString();
        }

        return new DeleteRequest(dbName + "_" + index_structure.name, index_structure.name, id);
    }

    private void searchAndReindexByOneLevel(RequestCollection bulkRequest, Long entity_id, Long revision_id, String entity_type, IndexStructure index_structure)
        throws SQLException, ParseException, ExecutionException, JsonProcessingException
    {
        //Send the request & get a list of entities in need of update
        List<String> paths = findCollectionsByType(index_structure.writingStructure, entity_type);
        searchAndReindex(bulkRequest, entity_id, revision_id, index_structure, paths);
    }

    private void searchAndReindexByTwoLevels(RequestCollection bulkRequest, Long entity_id, Long revision_id, String entity_type_lvl1, String entity_type_lvl2, IndexStructure index_structure)
        throws SQLException, ParseException, ExecutionException, JsonProcessingException
    {
        //Send the request & get a list of entities in need of update
        List<String> paths = findCollectionsByTwoLevelsOfType(index_structure.writingStructure, entity_type_lvl1, entity_type_lvl2);
        searchAndReindex(bulkRequest, entity_id, revision_id, index_structure, paths);
    }

    private void searchAndReindex(RequestCollection bulkRequest, Long entity_id, Long revision_id, IndexStructure index_structure, List<String> paths)
        throws SQLException, ParseException, ExecutionException, JsonProcessingException
    {
        if (paths.size() > 0) {
            BoolQueryBuilder query = QueryBuilders.boolQuery();

            for (String p : paths) {
                query = query
                    .should(QueryBuilders.nestedQuery(p, QueryBuilders.termQuery(p + ".entity_id", (long) entity_id)))
                    .should(QueryBuilders.nestedQuery(p, QueryBuilders.termQuery(p + ".revision_id", (long) revision_id)));
            }

            int total = 0;
            {
                SearchResponse response = es_client.prepareSearch(dbName + "_" + index_structure.name)
                    .setTypes(index_structure.name)
                    .setQuery(query)
                    .setSize(1)
//                    .setScroll(new TimeValue(100000l))
                    .execute()
                    .actionGet();

                SearchHits hits = response.getHits();
                total = (int)hits.totalHits();
            }

            if (total > 0) {
                SearchResponse response = es_client.prepareSearch(dbName + "_" + index_structure.name)
                    .setTypes(index_structure.name)
                    .setQuery(query)
                    .setSize(total + 10)
//                    .setScroll(new TimeValue(100000l))
                    .execute()
                    .actionGet();

                SearchHits hits = response.getHits();
                for (int i = 0; i < total; i++) {
                    String id = hits.getAt(i).getId();
                    if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Latest) {
                        UpdateRequest ur = insertEntityIntoIndex(Long.parseLong(id), 0l, index_structure);
                        if (ur != null) {
                            bulkRequest.add(ur);
                        }
                    }
                    else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.LatestActive) {
                        UpdateRequest ur = insertEntityIntoIndex(Long.parseLong(id), 0l, index_structure);
                        if (ur != null) {
                            bulkRequest.add(ur);
                        }
                    }
                    else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Revision) {
                        UpdateRequest ur = insertEntityIntoIndex(0l, Long.parseLong(id), index_structure);
                        if (ur != null) {
                            bulkRequest.add(ur);
                        }
                    }
                }
            }
        }
    }

    private List<String> findCollectionsByType(WritingStructure es, String entity_type)
    {
        List<String> local = new ArrayList<>();

        for (Map.Entry<String, WritingStructure> c : es.collections.entrySet()) {
            WritingStructure is = c.getValue();
            if (is.entity_type != null && is.entity_type.equals(entity_type)) {
                local.add("collections." + c.getKey());
            }
            List<String> inner = findCollectionsByType(is, entity_type);
            for (String in : inner) {
                local.add("collections." + c.getKey() + "." + in);
            }
        }

        return local;
    }

    private List<String> findCollectionsByTwoLevelsOfType(WritingStructure es, String entity_type_lvl1, String entity_type_lvl2)
    {
        List<String> local = new ArrayList<>();

        for (Map.Entry<String, WritingStructure> c : es.collections.entrySet()) {
            WritingStructure is = c.getValue();
            if (is.entity_type != null && is.entity_type.equals(entity_type_lvl1)) {
                for (Map.Entry<String, WritingStructure> c2 : es.collections.entrySet()) {
                    WritingStructure is2 = c.getValue();
                    if (is2.entity_type != null && is2.entity_type.equals(entity_type_lvl2)) {
                        local.add("collections." + c.getKey());
                        break;
                    }
                }
            }
            List<String> inner = findCollectionsByTwoLevelsOfType(c.getValue(), entity_type_lvl1, entity_type_lvl2);
            for (String in : inner) {
                local.add("collections." + c.getKey() + "." + in);
            }
        }

        return local;
    }

    private UpdateRequest insertEntityIntoIndex(Long entity_id, Long revision_id, IndexStructure index_structure)
        throws SQLException, org.json.simple.parser.ParseException, ExecutionException, JsonProcessingException
    {
        Entity e = null;
        String id = "";

        if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Latest) {
            e = mapper.getEntity(entity_id, index_structure.readingStructure, Mapper.ExtractionState.LatestActiveOrRemoved, Mapper.LockType.NoLock);
            id = entity_id.toString();
        }
        else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.LatestActive) {
            e = mapper.getEntity(entity_id, index_structure.readingStructure, Mapper.ExtractionState.LatestActive, Mapper.LockType.NoLock);
            id = entity_id.toString();
        }
        else if (index_structure.readingStructure.revision_state == Structure.RevisionStateType.Revision) {
            e = mapper.getRevision(revision_id, index_structure.readingStructure, Mapper.LockType.NoLock);
            id = revision_id.toString();
        }

        UpdateRequest updateRequest = null;

        if (e != null) {
            //The revision_id should be the same as the message's. Otherwise it's not the correct record.
            if (e.revision_id < revision_id) {
                logger.error("Incorrect record extracted, expected a revision_id of at least {}, but extracted {}!", revision_id, e.revision_id);
            }

            boolean proceed = true;

            if (index_structure.script_name != null) {
                //TODO: Load and execute `script_name`
            }

            if (proceed) {
                JSONObject restructured = translateEntityToEsStructure(e);
                String json = restructured.toString();

                final IndexRequest indexRequest = new IndexRequest(dbName + "_" + index_structure.name, index_structure.name, id)
                    .source(json);

                updateRequest = new UpdateRequest(dbName + "_" + index_structure.name, index_structure.name, id)
                    .doc(json)
                    .upsert(indexRequest);
            }
        }

        return updateRequest;
    }

    private JSONObject translateEntityToEsStructure(Entity e)
    {
        JSONObject r = new JSONObject();

        if (e.object_id != null) {
            r.put("revision_id", e.object_id);
        }
        if (e.revision_id != null) {
            r.put("revision_id", e.revision_id);
        }
        if (e.entity_id != null) {
            r.put("entity_id", e.entity_id);
        }
        if (e.creator != null) {
            r.put("creator", e.creator);
        }
        if (e.reviser != null) {
            r.put("reviser", e.reviser);
        }
        if (e.create_date != null) {
            r.put("create_date", datetime_format.format(e.create_date));
        }
        if (e.revise_date != null) {
            r.put("revise_date", datetime_format.format(e.revise_date));
        }

        JSONObject fields = new JSONObject();
        for (Map.Entry<String, Object> f : e.fields.entrySet()) {
            fields.put(f.getKey(), f.getValue());
        }
        r.put("fields", fields);

        JSONObject collections = new JSONObject();
        for (Map.Entry<String, List<Entity>> c : e.collections.entrySet()) {
            JSONArray new_collection = new JSONArray();
            for (Entity _e : c.getValue()) {
                JSONObject obj = translateEntityToEsStructure(_e);
                new_collection.add(obj);
            }
            collections.put(c.getKey(), new_collection);
        }
        r.put("collections", collections);

        return r;
    }
}
