package org.acidbase.ei.job;

public interface Job
{
    void execute(String json) throws Exception;
}
