package org.acidbase.ei;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;

import java.util.ArrayList;
import java.util.List;

public class RequestCollection
{
    private List<Request> requests = new ArrayList<>();

    public void add(IndexRequest ir)
    {
        requests.add(new Request(ir));
    }

    public void add(UpdateRequest ur)
    {
        requests.add(new Request(ur));
    }

    public void add(DeleteRequest dr)
    {
        requests.add(new Request(dr));
    }

    public void transferTo(BulkRequestBuilder bulkRequest)
    {
        transferTo(bulkRequest, 0);
    }

    public int size()
    {
        return requests.size();
    }

    public void transferTo(BulkRequestBuilder bulkRequest, int start)
    {
        for (int i = start; i < requests.size(); i++) {
            Request r = requests.get(i);
            if (r.ir != null) {
                bulkRequest.add(r.ir);
            }
            else if (r.ur != null) {
                bulkRequest.add(r.ur);
            }
            else if (r.dr != null) {
                bulkRequest.add(r.dr);
            }
        }
    }

    static private class Request
    {
        IndexRequest ir;
        UpdateRequest ur;
        DeleteRequest dr;

        public Request(IndexRequest ir)
        {
            this.ir = ir;
        }

        public Request(UpdateRequest ur)
        {
            this.ur = ur;
        }

        public Request(DeleteRequest dr)
        {
            this.dr = dr;
        }
    }
}
