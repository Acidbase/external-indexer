package org.acidbase.ei;

import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.Connector;
import org.acidbase.data.mapper.Mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataMapper extends Mapper
{
    public DataMapper(Connector connector)
    {
        super(connector);
    }

    public String getNotifyCommandData(Long notifyId)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("SELECT \"data\" FROM \"Acidbase\".notify_command WHERE id = ? FOR UPDATE");
            pst.setLong(1, notifyId);
            ResultSet rs = pst.executeQuery();
            String data = null;
            if (rs.next()) {
                data = rs.getString(1);
            }
            rs.close();
            pst.close();
            commit(key);
            return data;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public int removeNotifyCommand(Long notifyId)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("DELETE FROM \"Acidbase\".notify_command WHERE id = ?");
            pst.setLong(1, notifyId);
            int affected = pst.executeUpdate();
            pst.close();
            commit(key);
            return affected;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }
}
