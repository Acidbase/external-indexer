package org.acidbase.ei;

import com.rabbitmq.client.*;
import org.acidbase.data.ServerInfo;
import org.acidbase.ei.cache.ConnectorCache;
import org.acidbase.ei.job.Job;
import org.acidbase.ei.job.ReindexJob;
import org.acidbase.ei.job.ReloadStructuresJob;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class Main
{
    public static boolean PROFILE = false;
    static String PROFILE_OUTPUT = "";
    static String RMQ_HOST = null;
    static int RMQ_PORT = 0;
    static String RMQ_USER = null;
    static String RMQ_PASS = null;
    static String RMQ_EXCHANGE_WORKER = null;
    static String RMQ_QUEUE_WORKER = null;
    static String RMQ_EXCHANGE_BROADCAST = null;
    static String RMQ_ROUTE_KEY_INDEX_ENTITY = null;
    static String RMQ_ROUTE_KEY_RELOAD_STRUCTURE = null;
    static Properties prop;

    public static ServerInfo dbConnectionCreator;
    public static Map<String, Long> profileTimestamps = new HashMap<>();
    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] argv)
        throws Exception
    {
        InputStream input = null;
        input = Main.class.getClassLoader().getResourceAsStream("config.properties");
        if (input == null) {
            logger.error("Unable to find \"config.properties\"");
            return;
        }

        prop = new Properties();
        prop.load(input);

        PROFILE = prop.getProperty("app.profile").trim().equals("1");
        PROFILE_OUTPUT = prop.getProperty("app.profile_output").trim();

        RMQ_HOST = prop.getProperty("rmq.host").trim();
        RMQ_PORT = Integer.parseInt(prop.getProperty("rmq.port").trim());
        RMQ_USER = prop.getProperty("rmq.username").trim();
        RMQ_PASS = prop.getProperty("rmq.password").trim();
        RMQ_EXCHANGE_WORKER = prop.getProperty("rmq.exchange.worker").trim();
        RMQ_QUEUE_WORKER = prop.getProperty("rmq.queue.worker").trim();
        RMQ_EXCHANGE_BROADCAST = prop.getProperty("rmq.exchange.broadcast").trim();
        RMQ_ROUTE_KEY_INDEX_ENTITY = prop.getProperty("rmq.route_key.index_entity").trim();
        RMQ_ROUTE_KEY_RELOAD_STRUCTURE = prop.getProperty("rmq.route_key.reload_structure").trim();

        //==============================================================================================================
        //RabbitMQ setup
        //==============================================================================================================
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RMQ_HOST);
        factory.setPort(RMQ_PORT);
        if (!RMQ_USER.equals("")) {
            factory.setUsername(RMQ_USER);
        }
        if (!RMQ_PASS.equals("")) {
            factory.setUsername(RMQ_PASS);
        }
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        //Setting up the exchange and queue
        channel.exchangeDeclare(RMQ_EXCHANGE_WORKER, "direct", true, false, null);
        channel.queueDeclare(RMQ_QUEUE_WORKER, true, false, false, null);
        channel.queueBind(RMQ_QUEUE_WORKER, RMQ_EXCHANGE_WORKER, RMQ_ROUTE_KEY_INDEX_ENTITY);
        channel.exchangeDeclare(RMQ_EXCHANGE_BROADCAST, "fanout", false, false, null);
        String bcQueueName = channel.queueDeclare().getQueue();
        logger.info("Broadcast Queue Name: " + bcQueueName);
        channel.queueBind(bcQueueName, RMQ_EXCHANGE_BROADCAST, "");
        channel.basicQos(1);

        logger.info(" [*] Waiting for messages. To exit press CTRL+C");


        //==============================================================================================================
        //Preparing the PostgreSQL connection
        //==============================================================================================================
        dbConnectionCreator = new ServerInfo(
            prop.getProperty("db.protocol").trim(),
            prop.getProperty("db.type").trim(),
            prop.getProperty("db.host").trim(),
            Integer.parseInt(prop.getProperty("db.port").trim()),
            prop.getProperty("db.name").trim(),
            prop.getProperty("db.user").trim(),
            prop.getProperty("db.pass").trim()
        );
        ConnectorCache.setServerInfo(dbConnectionCreator);


        //==============================================================================================================
        //Preparing the Elasticsearch connection
        //==============================================================================================================
        Settings es_settings = Settings
            .settingsBuilder()
            .put("cluster.name", prop.getProperty("es.cluster_name").trim())
//            .put("node.name", prop.getProperty("es.node_name").trim())
            .build();
        Client es_client = TransportClient
            .builder()
            .settings(es_settings)
            .build()
            .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(prop.getProperty("es.host").trim()), Integer.parseInt(prop.getProperty("es.port").trim())));

        //==============================================================================================================
        //Populating job handlers
        //==============================================================================================================
        Map<String, Job> jobs = new HashMap<>();
        jobs.put(RMQ_ROUTE_KEY_RELOAD_STRUCTURE, new ReloadStructuresJob());
        jobs.put(RMQ_ROUTE_KEY_INDEX_ENTITY, new ReindexJob(es_client));

        final DefaultConsumer consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                throws IOException
            {
                String message = new String(body, "UTF-8");
                logger.info(" [x] Received with key `{}`: `{}`", envelope.getRoutingKey(), message);
                try {
                    boolean processed = false;
                    for (Map.Entry<String, Job> j : jobs.entrySet()) {
                        if (j.getKey().equals(envelope.getRoutingKey())) {
                            j.getValue().execute(message);
                            processed = true;
                        }
                    }
                    if (!processed) {
                        writeDownProfiling();
                    }
                }
                catch (Exception e) {
                    logger.error(e.toString());
                    e.printStackTrace();
                }
                finally {
                    logger.info(" [x] Done");
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }

            @Override
            public void handleShutdownSignal(java.lang.String consumerTag, ShutdownSignalException sig)
            {
                writeDownProfiling();
                es_client.close();
                logger.info(" [x] Bye bye");
            }
        };
        channel.basicConsume(RMQ_QUEUE_WORKER, false, consumer);
        channel.basicConsume(bcQueueName, false, consumer);
    }

    static private void writeDownProfiling()
    {
        if (PROFILE) {
            BufferedWriter writer = null;
            try {
                File profileFile = new File(PROFILE_OUTPUT + "." + UUID.randomUUID().toString());
                writer = new BufferedWriter(new FileWriter(profileFile));
                for (Map.Entry<String, Long> entry : profileTimestamps.entrySet()) {
                    writer.write(entry.getKey() + " " + entry.getValue().toString() + "\n");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                }
                catch (Exception ignored) {}
            }
        }
    }
}
