package org.acidbase.ei;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.acidbase.data.ServerInfo;
import org.acidbase.data.mapper.Connector;

import java.sql.Connection;
import java.sql.SQLException;

public class HikariConnector implements Connector
{
    private ServerInfo serverInfo;
    private HikariDataSource ds;

    public HikariConnector(ServerInfo serverInfo)
    {
        this.serverInfo = serverInfo;

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(serverInfo.getUrl());
        config.setUsername(serverInfo.getUsername());
        config.setPassword(serverInfo.getPassword());
        config.setConnectionTimeout(Long.parseLong(Main.prop.getProperty("cp.connectionTimeout")));
        config.setIdleTimeout(Long.parseLong(Main.prop.getProperty("cp.idleTimeout")));
        config.setMaxLifetime(Long.parseLong(Main.prop.getProperty("cp.maxLifetime")));
        config.setMinimumIdle(Integer.parseInt(Main.prop.getProperty("cp.minimumIdle")));
        config.setMaximumPoolSize(Integer.parseInt(Main.prop.getProperty("cp.maximumPoolSize")));

        ds = new HikariDataSource(config);
    }

    @Override
    public String getIdentifier()
    {
        return serverInfo.getUrl();
    }

    @Override
    public Connection newConnection()
            throws SQLException
    {
        return ds.getConnection();
    }

    @Override
    public void close()
    {
        ds.close();
    }

    @Override
    public boolean isClosed()
    {
        return ds.isClosed();
    }
}
