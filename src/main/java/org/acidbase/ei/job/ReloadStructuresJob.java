package org.acidbase.ei.job;

import org.acidbase.data.IndexStructureCache;
import org.acidbase.data.mapper.Connector;
import org.acidbase.ei.cache.ConnectorCache;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReloadStructuresJob implements Job
{
    static Logger logger = LoggerFactory.getLogger(ReloadStructuresJob.class);

    public ReloadStructuresJob()
    {
    }

    @Override
    public void execute(String json)
        throws Exception
    {
        JSONParser parser = new JSONParser();
        JSONObject payload = (JSONObject) parser.parse(json);

        Connector connector = ConnectorCache.getConnector(payload.get("db").toString());
        IndexStructureCache.invalidateInstance(connector);

        logger.info(" [x] Reloaded structures for database `{}`", payload.get("db").toString());
    }
}
